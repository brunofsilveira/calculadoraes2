/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package calculadora;
import java.util.*;
/**
 *
 * @author bruno-ferraz-silveira
 */
public class Calculadora {

    public double somar(double v1, double v2) {
        double v3 = v1 + v2;
        
        return v3;
    }

    public double subtrair(double v1, double v2) {
        
        return v1 - v2;
    
    }

    public double multiplicar(double v1, double v2) {
        double v3 = v1 * v2;        
        
        return v3;
    }

    public double dividir(double v1, double v2) {
        double v3 = v1 / v2;
        
        return v3;
    }

    public double potenciacao(double v1, double v2) {
        double v3 = Math.pow(v1, v2);

        return v3;
    }

}
